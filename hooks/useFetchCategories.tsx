import React, { useState, useEffect } from 'react'
import { getCategories } from '@/services';
import { Category } from '@/utils/utils';

function useFetchCategories() {
    const [categories, setCategories] = useState<Category[]>([]);

    useEffect(() => {
        getCategories().then(response => setCategories(response));
    }, [])

    return categories;
}

export default useFetchCategories;