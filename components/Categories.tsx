import React from 'react'
import Link from 'next/link';
import { Category } from '@/utils/utils';
import useFetchCategories from '@/hooks/useFetchCategories';

export const Categories = () => {

    const categories = useFetchCategories();

    return (
        <div className="bg-white shadow-lg rounded-lg p-8 pb-12 mb-8">
            <h3 className='text-xl mb-8 font-semibold border-b pb-4'>Categories</h3>
            {categories && categories?.map((category: Category) => (
                <Link key={category?.id} href={`/category/${category?.slug}`}>
                    <span className='cursor-pointer block pb-3 mb-3'>
                        {category?.name}
                    </span>
                </Link>
            ))}
        </div>
    )
}

export default Categories;
