import React, { useState, useEffect } from 'react'
import Link from 'next/link';
import { getRecentPosts, getSimilarPosts } from '../services';
import { formatDate, Post } from '@/utils/utils';

interface PostWidgetProps {
    slug?: string
    categories?: string[]
}

export const PostWidget = (props: PostWidgetProps) => {
    const { categories, slug } = props;
    const [relatedPosts, setRelatedPosts] = useState<Post[]>([]);

    useEffect(() => {
        if (slug) {
            getSimilarPosts(categories, slug).then(response => setRelatedPosts(response))
        }
        else {
            getRecentPosts().then(response => setRelatedPosts(response));
        }
    }, [slug])

    return (
        <div className="bg-white shadow-lg rounded-lg p-8 pb-12 mb-8">
            <h3 className='text-xl mb-8 font-semibold border-b pb-4'>{slug ? "Related Post " : "Recent Posts"}</h3>
            {relatedPosts && relatedPosts?.map((post: Post) => (
                <div className='flex items-center w-full mb-4' key={post?.title}>
                    <div className='w-16 flex-none'>
                        <img src={post?.featuredImage?.url} alt={post?.title} height="60px" width={"60px"} />
                    </div>
                    <div className='flex-grow ml-4'>
                        <p className='text-gray-500 text-xs'>{formatDate(post?.createdAt)}</p>
                        <Link href={`/post/${post.slug}`} className="text-md">{post.title}</Link>
                    </div>
                </div>
            ))}
        </div>
    )
}

export default PostWidget;
