import React, { ReactNode } from 'react'
import Header from './Header'

interface LayoutComponentProps {
    children: ReactNode
}

export const Layout = (props: LayoutComponentProps) => {
    const {children} = props;
    return (
        <>
            <Header />
            {children}
        </>
    )
}

export default Layout;
