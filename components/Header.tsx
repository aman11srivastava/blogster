import React from 'react'
import Link from 'next/link';
import useFetchCategories from '@/hooks/useFetchCategories';
import { Category } from '@/utils/utils';

export const Header = () => {
    const categories = useFetchCategories();

    return (
        <div className='container mx-auto px-10 mb-8'>
            <div className='border-b w-full inline-block border-blue-400 py-8'>
                <div className='md:float-left block'>
                    <Link href={"/"}>
                        <span className="cursor-pointer font-bold text-4xl text-white">
                            Blogster
                        </span>
                    </Link>
                </div>
                <div className="hidden md:float-left md:contents">
                    {categories?.map((category: Category) => (
                        <Link key={category.name} href={`/category/${category.slug}`}>
                            <span className='md:float-right mt-2 align-middle text-white ml-4 font-semibold cursor-pointer'>
                                {category.name}
                            </span>
                        </Link>
                    ))}
                </div>
            </div>
        </div>
    )
}

export default Header;
