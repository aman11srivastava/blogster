import { graphqlAPI } from '../utils/utils';
import { request, gql } from 'graphql-request';
import dotenv from 'dotenv'

dotenv.config();

export const getPosts = async () => {
    const query = gql`
        query getPosts {
            postsConnection {
            edges {
                node {
                author {
                    bio
                    id
                    name
                    photo {
                        url
                        id
                    }
                }
                createdAt
                slug
                title
                excerpt
                id
                featuredImage {
                    url
                    id
                }
                categories {
                    name
                    slug
                    id
                }
                }
            }
            }
        }
    `;
    const results = await request(graphqlAPI, query);
    return results?.postsConnection?.edges;

}

export const getRecentPosts = async () => {
    const query = gql`
        query GetPostDetails() {
            posts(
                orderBy: createdAt_ASC
                last: 3
            ) {
                title
                id
                featuredImage {
                    url
                    id
                }
                createdAt
                slug
            }
        }
    `;
    const results = await request(graphqlAPI, query);
    return results?.posts;
}

export const getSimilarPosts = async (categories, slug) => {
    const query = gql`
      query GetPostDetails($slug: String!, $categories: [String!]) {
        posts(
            where: {slug_not: $slug, AND: {categories_some: {slug_in: $categories}}}
            last: 3
        ) {
            title
            featuredImage {
                url
                id
            }
            createdAt
            slug
        }
      }
    `;
    const result = await request(graphqlAPI, query, { slug, categories });

    return result.posts;
}

export const getCategories = async () => {
    const query = gql`
        query GetCategories {
            categories {
                name
                slug
                id
            }
        }
    `;
    const results = await request(graphqlAPI, query);
    return results?.categories;
}