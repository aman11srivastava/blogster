import moment from "moment";

export const graphqlAPI = process.env.NEXT_PUBLIC_ENDPOINT;

export type PostType = {
    node: Post
}

export interface Author {
    name: string
    id: string
    bio: string
    photo: Image
}

export interface Image {
    url: string
    id: string
}

export interface Category {
    name: string
    slug: string
    id: string
}

export interface Post {
    author: Author
    createdAt: Date
    slug: string
    title: string
    excerpt: string
    id: string
    featuredImage: Image
    categories: Category[]
}

export function formatDate(date: Date): string {
    return moment(date).format("MMM DD, YYYY")
}