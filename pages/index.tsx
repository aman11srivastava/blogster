import Head from 'next/head'
import { GetStaticProps, InferGetStaticPropsType } from 'next';
import { Categories, PostCard, PostWidget } from '../components';
import { getPosts } from '../services';
import { PostType } from '../utils/utils'

export default function Home({ posts }: InferGetStaticPropsType<typeof getStaticProps>) {
  return (
    <div className='container mx-auto px-10 mb-8'>
      <Head>
        <title>Blogster</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className='grid grid-cols-1 lg:grid-cols-12 gap-12'>
        <div className='lg:col-span-8 col-span-1'>
          {posts?.map((post: PostType, index: number) => <PostCard post={post?.node} key={index} />)}
        </div>

        <div className='lg:col-span-4 col-span-1'>
          <div className='lg:sticky relative top-8'>
            <PostWidget />
            <Categories />
          </div>
        </div>
      </div>
    </div>
  )
}

export const getStaticProps: GetStaticProps<{ posts: PostType[] }> = async () => {
  const posts = (await getPosts()) || [];
  return {
    props: { posts }
  }
}